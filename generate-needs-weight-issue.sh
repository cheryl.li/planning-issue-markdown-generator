#!/bin/bash

generate_issue_links() {
  curl -s "https://gitlab.com/api/v4/groups/gitlab-org/issues?state=opened&per_page=100&labels=group::continuous%20integration,$1" | jq '.[] | "* [ ] [\(.title)](\(._links.self))"' | sed "s/\"//g"
}

usage()
{
    echo "usage: ./generate-needs-weight-issue.sh [[[-c candidate ] | [-l labels (csv) ] | [-h]]"
    echo "  e.g. ./generate-needs-weight-issue.sh -c 13.4"
    echo "   or  ./generate-needs-weight-issue.sh -l needs%20weight,candidate::13.4"
}

##### Main

while [ "$1" != "" ]; do
    case $1 in
        -c | --candidate )      shift
                                generate_issue_links "needs%20weight,candidate::$1"
                                ;;
        -l | --labels )         shift
                                generate_issue_links $1
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done

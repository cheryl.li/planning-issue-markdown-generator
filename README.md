# Generate markdown for the Needs Weight issue

Generates the linked issues in markdown that can be added to Needs Weight issues. It uses the following syntax to execute a bash command (calls the API via curl and parses the json response with `jq` accordingly) to retrieve an issue's:
 * title
 * url

```
./generate-needs-weight-issue.sh [[[-c milestone candidate ] | [-l labels (csv) ] | [-h]
```

Example Usage:

```
./generate-needs-weight-issue.sh -c 13.4
* [ ] [Improve the navigation between parent-child pipelines](https://gitlab.com/api/v4/projects/278964/issues/229503)
* [ ] [Document migration from GitHub Actions to GitLab CI/CD](https://gitlab.com/api/v4/projects/278964/issues/228937)
* [ ] [With approval by Merge Request authors disabled, approval incorrectly states \Merge request approved.\ when it has not been](https://gitlab.com/api/v4/projects/278964/issues/228924)
(...etc))
```
*assumes the `candidate::[milestone]` label is being used*

```
./generate-needs-weight-issue.sh -l (csv list of labels)
```
*note that `group::continuous integration` label is always present by default*

